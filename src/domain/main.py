import sys

## Usuário ##
class User:
    def __init__(self, name):
        self._name = name

    @property
    def name(self):
        return self._name

## Lances ##
class Bid:
    def __init__(self, user, money_value):
        self._user = user
        self._money_value = money_value

## Leilão ##
class Auction:
    def __init__(self, description):
        self._description = description
        self._bids = []
        self._highest_bid = sys.float_info.min
        self._lowest_bid = sys.float_info.max

    def propose(self, bid: Bid):
        if not self._bids or self._bids[-1]._user != bid._user and bid._money_value > self._bids[-1]._money_value:
            if bid._money_value > self._highest_bid:
                self._highest_bid = bid._money_value
            if bid._money_value < self._lowest_bid:
                self._lowest_bid = bid._money_value

            self._bids.append(bid)
        else:
            raise ValueError('Error to propose a bid')

    @property
    def bids(self):
        return self._bids[:]
