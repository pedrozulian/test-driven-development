from domain.main import User, Bid, Auction, Reviewer

_user_charlie = User('Charlie')
_user_maggie = User('Maggie')

# print(f'User in auction: \n{_user_charlie.name} \n{_user_maggie.name}')

_bid_of_charlie = Bid(_user_charlie, 1000.0)
_bid_of_maggie = Bid(_user_maggie, 1250.0)

_auction = Auction('Auction number: 001')
_auction._bids.append(_bid_of_maggie)
_auction._bids.append(_bid_of_charlie)

# for bid in _auction._bids:
#     print(f'User {bid._user._name} make a bid of ${bid._money_value}')

_review = Reviewer()
_review.appraise(_auction)
# print(f'The lowest bid was {_review._lowest_bid}\nThe highest bid was {_review._highest_bid}')
