import unittest
from src.domain.main import User, Auction, Bid

class TestMain(unittest.TestCase):

    def setUp(self):
        self._user_charlie = User('Charlie')
        self._user_maggie = User('Maggie')
        self._bid_of_charlie = Bid(self._user_charlie, 1000.0)
        self._bid_of_maggie = Bid(self._user_maggie, 1250.0)
        self._auction = Auction('iPhone X')
    
    def test_appraise_adding_bids_on_ascending_order(self):
        self._auction.propose(self._bid_of_charlie)
        self._auction.propose(self._bid_of_maggie)

        self.assertEqual(1000.0, self._auction._lowest_bid)
        self.assertEqual(1250.0, self._auction._highest_bid)

    def test_appraise_adding_bids_on_descending_order(self):
        with self.assertRaises(ValueError):
            self._auction.propose(self._bid_of_maggie)
            self._auction.propose(self._bid_of_charlie)

    def test_appraise_when_have_more_than_three_bids(self):
        _user_karim = User('Karim')
        _bid_of_karim = Bid(_user_karim, 910.0)

        self._auction.propose(_bid_of_karim)
        self._auction.propose(self._bid_of_charlie)
        self._auction.propose(self._bid_of_maggie)

        self.assertEqual(910.0, self._auction._lowest_bid)
        self.assertEqual(1250.0, self._auction._highest_bid)


    def test_auction_without_bids_allow_first_bid(self):
        self._auction.propose(self._bid_of_charlie)

        self.assertEqual(1, len(self._auction._bids))

    def test_allow_user_when_last_user_was_different(self):
        _user_kate = User('Kate')
        _bid_of_kate = Bid(_user_kate, 1100.0)

        self._auction.propose(_bid_of_kate)
        self._auction.propose(self._bid_of_maggie)
        
        self.assertEqual(2, len(self._auction._bids))

    def test_dont_allow_bids_in_sequence_of_same_user(self):
        _bid_of_charlie_another = Bid(self._user_charlie, 1300.0)

        with self.assertRaises(ValueError):
            self._auction.propose(self._bid_of_charlie)
            self._auction.propose(_bid_of_charlie_another)
